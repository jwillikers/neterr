neterr
===
neterr is a simple extraction of the net package's behavior-based error handling.

Getting Started
---
The following section is all you need to use neterr in your code.

### Behavior
There are two different methods to report the behavior of an error.
1. Temporary - returns true if the error is ephemeral and not fatal
2. Timeout - returns true if the error is due to a timeout

### Creating Errors
Errors can be created conveniently by either wrapping an existing error or providing an error message for a new error. The neterr package encapsulates these behavior-driven errors with the NetError struct.

#### New
When New is used in the function's name, then the first argument will be a string, the error message. The method will take care of creating the underlying error using this string.

The bare New function returns a NetError created from the given message, with default timeout and temporary behavior's as false.
```go
err := neterr.New("this is a default error")
err.Timeout()   // false
err.Temporary() // false
```

There are other functions provided for convenient creation of NetErrors with Temporary behavior, Timeout behavior, and even both behaviors.
```go
err := neterr.NewTimeout("this is a timeout error")
err.Timeout()   // true
err.Temporary() // false
err = neterr.NewTemporary("this is a temporary error")
err.Timeout()   // false
err.Temporary() // true
err = neterr.NewTemporaryTimeout("this is a temporary error")
err.Timeout()   // true
err.Temporary() // true
```

#### Wrapping
When Wrap is used in the function's name, then the first argument will be an error, the base error for the NetError struct. Wrap will also opt to use the underlying error's behavior if allowed.

The following example shows how by wrapping an error, it's behavior can be inherited from the provided error.
```go
err := neterr.Wrap(neterr.NewTemporary("this is a temporary error"))
err.Temporary() // true
```

Additionally, when wrapping an error, its behavior can be over-written as seen below.
```go
err := neterr.WrapTemporary(neterr.NewTemporary("this is a temporary error at first"), false)
err.Temporary() // false
```

### In the Wild
-   An example that currently uses neterr's Error types is the [sirius](https://bitbucket.org/athrun22/sirius/src/develop/) project.

Based On
---
-   [net](https://golang.org/pkg/net/) - provides a portable interface for network I/O, including TCP/IP, UDP, domain name resolution, and Unix domain sockets.

Versioning
---
[SemVer](http://semver.org/) is used for versioning. For the versions available, see the tags on this repository.

Contributing
---
Please read [CONTRIBUTING.md](./CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

Authors
---
-   Jordan Williams

License
---
This project is licensed under the MIT License - see the [LICENSE.md](./LICENSE.md) file for details
