package neterr

// Error is for tracking errors and determining the behavior of the errors.
//
// This interface is taken directly from the net package, but not imported to avoid additional dependencies.
type Error interface {
	error
	Timeout() bool   // Is the error due to a timeout?
	Temporary() bool // Is the error only temporary?
}
