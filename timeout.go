package neterr

// timeout represents an error that reports whether it is due to a timeout or not.
type timeout interface {
	Timeout() bool
}
