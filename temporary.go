package neterr

// temporary represents an error that reports whether it is temporary or not.
type temporary interface {
	Temporary() bool
}
