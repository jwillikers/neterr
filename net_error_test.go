package neterr

import (
	"errors"
	"testing"
)

func TestNew(t *testing.T) {
	msg := "underlying"
	err := New(msg)
	if nil == err {
		t.Error("the new error should not be nil")
	}
	if msg != err.err.Error() {
		t.Errorf("the error message should be '%v' not '%v'", msg, err.err.Error())
	}
}

func TestWrap(t *testing.T) {
	base := errors.New("underlying")
	err := Wrap(base)
	if nil == err {
		t.Error("the new error should not be nil")
	}
	if base.Error() != err.err.Error() {
		t.Errorf("the error message should be '%v' not '%v'", base, err.err)
	}
}

func TestError(t *testing.T) {
	base := errors.New("underlying")
	err := &NetError{base, false, false, false, false}
	if base.Error() != err.Error() {
		t.Errorf("the error message should be '%v' not '%v'", base, err)
	}
}

func TestNewTemporary(t *testing.T) {
	msg := "this is a temporary error"
	netErr := NewTemporary(msg)
	if nil == netErr {
		t.Error("the new error should not be nil")
	}
	if msg != netErr.Error() {
		t.Errorf("the error message should be '%v' not '%v'", msg, netErr)
	}
	if !netErr.setTemporary {
		t.Errorf("setTemporary should be true")
	}
	if !netErr.temporary {
		t.Errorf("temporary should be true")
	}
}

func TestWrapTemporary(t *testing.T) {
	t.Run("true", func(t *testing.T) {
		base := errors.New("this is a temporary error")
		netErr := WrapTemporary(base, true)
		if nil == netErr {
			t.Error("the new error should not be nil")
		}
		if base.Error() != netErr.Error() {
			t.Errorf("the error message should be '%v' not '%v'", base, netErr)
		}
		if !netErr.setTemporary {
			t.Errorf("setTemporary should be true")
		}
		if !netErr.temporary {
			t.Errorf("temporary should be true")
		}
	})
	t.Run("false", func(t *testing.T) {
		base := errors.New("this is not a temporary error")
		netErr := WrapTemporary(base, false)
		if nil == netErr {
			t.Error("the new error should not be nil")
		}
		if base.Error() != netErr.Error() {
			t.Errorf("the error message should be '%v' not '%v'", base, netErr)
		}
		if !netErr.setTemporary {
			t.Errorf("setTemporary should be true")
		}
		if netErr.temporary {
			t.Errorf("temporary should be false")
		}
	})
}

func TestNewTimeout(t *testing.T) {
	msg := "this is a timeout error"
	netErr := NewTimeout(msg)
	if nil == netErr {
		t.Error("the new error should not be nil")
	}
	if msg != netErr.Error() {
		t.Errorf("the error message should be '%v' not '%v'", msg, netErr)
	}
	if !netErr.setTimeout {
		t.Errorf("setTimeout should be true")
	}
	if !netErr.timeout {
		t.Errorf("timeout should be true")
	}
}

func TestWrapTimeout(t *testing.T) {
	t.Run("true", func(t *testing.T) {
		base := errors.New("this is a timeout error")
		netErr := WrapTimeout(base, true)
		if nil == netErr {
			t.Error("the new error should not be nil")
		}
		if base.Error() != netErr.Error() {
			t.Errorf("the error message should be '%v' not '%v'", base, netErr)
		}
		if !netErr.setTimeout {
			t.Errorf("setTimeout should be true")
		}
		if !netErr.timeout {
			t.Errorf("timeout should be true")
		}
	})
	t.Run("false", func(t *testing.T) {
		base := errors.New("this is not a timeout error")
		netErr := WrapTimeout(base, false)
		if nil == netErr {
			t.Error("the new error should not be nil")
		}
		if base.Error() != netErr.Error() {
			t.Errorf("the error message should be '%v' not '%v'", base, netErr)
		}
		if !netErr.setTimeout {
			t.Errorf("setTimeout should be true")
		}
		if netErr.timeout {
			t.Errorf("timeout should be false")
		}
	})
}

func TestNewTemporaryTimeout(t *testing.T) {
	msg := "this is both a temporary and timeout error"
	netErr := NewTemporaryTimeout(msg)
	if nil == netErr {
		t.Error("the new error should not be nil")
	}
	if msg != netErr.Error() {
		t.Errorf("the error message should be '%v' not '%v'", msg, netErr)
	}
	if !netErr.setTemporary {
		t.Errorf("setTemporary should be true")
	}
	if !netErr.temporary {
		t.Errorf("temporary should be true")
	}
	if !netErr.setTimeout {
		t.Errorf("setTimeout should be true")
	}
	if !netErr.timeout {
		t.Errorf("timeout should be true")
	}
}

func TestWrapTemporaryTimeout(t *testing.T) {
	t.Run("temporary", func(t *testing.T) {
		base := errors.New("this is a temporary error")
		netErr := WrapTemporaryTimeout(base, true, false)
		if nil == netErr {
			t.Error("the new error should not be nil")
		}
		if base.Error() != netErr.Error() {
			t.Errorf("the error message should be '%v' not '%v'", base, netErr)
		}
		if !netErr.setTemporary {
			t.Errorf("setTemporary should be true")
		}
		if !netErr.temporary {
			t.Errorf("temporary should be true")
		}
		if !netErr.setTimeout {
			t.Errorf("setTimeout should be true")
		}
		if netErr.timeout {
			t.Errorf("timeout should be true")
		}
	})
	t.Run("neither", func(t *testing.T) {
		base := errors.New("this is neither a temporary nor timeout error")
		netErr := WrapTemporaryTimeout(base, false, false)
		if nil == netErr {
			t.Error("the new error should not be nil")
		}
		if base.Error() != netErr.Error() {
			t.Errorf("the error message should be '%v' not '%v'", base, netErr)
		}
		if !netErr.setTemporary {
			t.Errorf("setTemporary should be true")
		}
		if netErr.temporary {
			t.Errorf("temporary should be false")
		}
		if !netErr.setTimeout {
			t.Errorf("setTimeout should be true")
		}
		if netErr.timeout {
			t.Errorf("timeout should be false")
		}
	})
}

func TestTemporary(t *testing.T) {
	t.Run("underlying=false", func(t *testing.T) {
		err := &NetError{errors.New("this is a temporary error"), false, false, false, false}
		if err.Temporary() {
			t.Error("Temporary should return false")
		}
	})
	t.Run("underlying=true", func(t *testing.T) {
		base := &NetError{errors.New("this is a temporary error"), true, true, false, false}
		err := &NetError{base, false, false, false, false}
		if !err.Temporary() {
			t.Error("Temporary should return true")
		}
	})
	t.Run("overwrite=true", func(t *testing.T) {
		err := &NetError{errors.New("this is a temporary error"), true, true, false, false}
		if !err.Temporary() {
			t.Error("Temporary should return true")
		}
	})
}

func TestTimeout(t *testing.T) {
	t.Run("underlying=false", func(t *testing.T) {
		err := &NetError{errors.New("this is a timeout error"), false, false, false, false}
		if err.Timeout() {
			t.Error("Timeout should return false")
		}
	})
	t.Run("underlying=true", func(t *testing.T) {
		base := &NetError{errors.New("this is a timeout error"), false, false, true, true}
		err := &NetError{base, false, false, false, false}
		if !err.Timeout() {
			t.Error("Timeout should return true")
		}
	})
	t.Run("overwrite=true", func(t *testing.T) {
		err := &NetError{errors.New("this is a timeout error"), false, false, true, true}
		if !err.Timeout() {
			t.Error("Timeout should return true")
		}
	})
}
