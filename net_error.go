package neterr

import "errors"

// NetError represents an error that can have its temporary status or timeout status set.
type NetError struct {
	err          error
	setTemporary bool
	temporary    bool
	setTimeout   bool
	timeout      bool
}

// New returns a new NetError with the given error message and behavior indicating it is neither a timeout nor temporary.
func New(msg string) *NetError {
	return &NetError{errors.New(msg), true, false, true, false}
}

// Wrap returns a new NetError that will use the given error to determine its temporary and timeout behavior.
func Wrap(err error) *NetError {
	return &NetError{err, false, false, false, false}
}

// NewTemporary creates a new NetError with the given error message and temporary behavior.
func NewTemporary(msg string) *NetError {
	return &NetError{errors.New(msg), true, true, true, false}
}

// WrapTemporary creates a new NetError with its base error's temporary status overridden by the given value.
func WrapTemporary(err error, temporary bool) *NetError {
	return &NetError{err, true, temporary, false, false}
}

// NewTimeout creates a new NetError with the given error message and timeout behavior.
func NewTimeout(msg string) *NetError {
	return &NetError{errors.New(msg), true, false, true, true}
}

// WrapTimeout creates a new NetError with its base error's timeout status overridden by the given value.
func WrapTimeout(err error, timeout bool) *NetError {
	return &NetError{err, false, false, true, timeout}
}

// NewTemporaryTimeout creates a new NetError with the given error message and both temporary and timeout behavior.
func NewTemporaryTimeout(msg string) *NetError {
	return &NetError{errors.New(msg), true, true, true, true}
}

// WrapTemporaryTimeout creates a new NetError with its base error's temporary and timeout statuses overridden by the given values.
func WrapTemporaryTimeout(err error, temporary bool, timeout bool) *NetError {
	return &NetError{err, true, temporary, true, timeout}
}

func (e *NetError) Error() string {
	return e.err.Error()
}

// Temporary will return true if the error is only temporary.
func (e *NetError) Temporary() bool {
	if e.setTemporary {
		return e.temporary
	}
	t, ok := e.err.(temporary)
	return ok && t.Temporary()
}

// Timeout will return true if the error is due to a timeout.
func (e *NetError) Timeout() bool {
	if e.setTimeout {
		return e.timeout
	}
	t, ok := e.err.(timeout)
	return ok && t.Timeout()
}
